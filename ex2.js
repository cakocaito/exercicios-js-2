function multiplicaMatrizes(matriz1, matriz2) {
    var matriz1NumeroLinhas = matriz1.length, matriz1NumeroColunas = matriz1[0].length,
        matriz2NumeroLinhas = matriz2.length, matriz2NumeroColunas = matriz2[0].length,
        m = new Array(matriz1NumeroLinhas);
    for (var r = 0; r < matriz1NumeroLinhas; ++r) {
        m[r] = new Array(matriz2NumeroColunas);
        for (var c = 0; c < matriz2NumeroColunas; ++c) {
            m[r][c] = 0;
            for (var i = 0; i < matriz1NumeroColunas; ++i) {
                m[r][c] += matriz1[r][i] * matriz2[i][c];
            }
        }
    }
    return m;
}

console.log(multiplicaMatrizes( [ [ [2],[-1] ], [ [2],[0] ] ],[ [2,3],[-2,1] ] ));
console.log(multiplicaMatrizes([[4,0],[-1,-1]], [[-1,3],[2,7]] ));